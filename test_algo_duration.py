# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
from os import walk

WINDOW = '15min'
#WINDOW = '30min'
#WINDOW = '1H'

DATA_FILE = "./outputs/eventDurationData/concat_%s/service_data_from_2022-05-24_to_2022-06-03_window_%s.pkl" % (WINDOW, WINDOW)

# NON-NOMINAL WEEK
VAL_FILE = "./outputs/eventDurationData/val_%s/service_data_from_2022-06-20_to_2022-06-25_window_%s.pkl" % (WINDOW, WINDOW)

# NOMINAL WEEK
#VAL_FILE = "service_data_from_2022-06-27_to_2022-06-29_window_%s.pkl" % WINDOW

            
files = []
for (dirpath, dirnames, filenames) in walk("./outputs/eventDurationData/concat_%s"%WINDOW):
    files.extend(filenames)
    break

df_list = []

for file in files:
    if file.endswith(".pkl"):
        print("loading %s"%file)
        df_list.append(pd.read_pickle("./outputs/eventDurationData/concat_%s/%s"%(WINDOW, file)))
            
def column_extract_max(df, data_acc):
    for index, row in df.iterrows():
        if not row['service'] in data_acc:
            data_acc[row['service']] = row['eventDuration']
        else:
            if data_acc[row['service']] < row['eventDuration']:
                data_acc[row['service']] = row['eventDuration']

# ============= MAIN ============= 
if __name__ == '__main__':
    
    data_acc = {}
    
    for df in df_list:
        column_extract_max(df, data_acc)
        
    x_services = []
    y_max_values = []
    for key, value in enumerate(data_acc):
        x_services.append(key)
        y_max_values.append(value)
    
    data_set = pd.read_pickle(DATA_FILE)
    data_val = pd.read_pickle(VAL_FILE)
    
    data_acc_val = {}
    column_extract_max(data_val, data_acc_val)
    
    anomalies_times = [
        '2022-06-20 14:00:00',
        '2022-06-20 15:15:00',
        '2022-06-22 18:15:00',
        '2022-06-23 21:15:00',
        '2022-06-24 7:00:00',
        '2022-06-24 7:15:00'
    ]
    
    near_anomalies_times = [
        ('2022-06-20 14:00:00', '2022-06-20 15:30:00'),
        ('2022-06-22 20:00:00', '2022-06-22 20:30:00'),
        ('2022-06-23 23:00:00', '2022-06-23 23:30:00'),
        ('2022-06-24 09:00:00', '2022-06-20 09:30:00')
    ]
    
    anomalies_slices = []
    anomalies_index = []
    for anomaly_time in anomalies_times:
        try:
            anomalies_slices.append(data_val.index.get_loc(anomaly_time))
        except:
            pass # Do nothing if no index found for the given time
            
    for index in anomalies_slices:
        for i in range(index.start, index.stop + 1):
            if not i in anomalies_index:
                anomalies_index.append(i)
                
    data_anomalies = {
        'service': [],
        'eventDuration': []
    }
    for index in anomalies_index:
        data_anomalies['service'].append(data_val.iloc[index]['service'])
        data_anomalies['eventDuration'].append(data_val.iloc[index]['eventDuration'])
        
    data_anomalies = pd.DataFrame(data_anomalies)
    
    # cut the right part
    data_set.drop(data_set[data_set['service'] > 60].index, inplace = True)
    data_val.drop(data_val[data_val['service'] > 60].index, inplace = True)
    data_anomalies.drop(data_anomalies[data_anomalies['service'] > 60].index, inplace = True)
    
    # plot data set
    ax = data_val.plot.scatter(x = 'service', y = 'eventDuration', c='blue')
    data_set.plot.scatter(x = 'service', y = 'eventDuration', ax=ax, c='green')
    data_anomalies.plot.scatter(x = 'service', y = 'eventDuration', ax=ax, c='red')
    
    plt.show()
