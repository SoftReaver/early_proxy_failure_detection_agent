# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin


class NRequestTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for row in X:
            row['nRequest'] = 1
        return X
