# -*- coding: utf-8 -*-
import numpy as np
import csv
from sklearn.decomposition import PCA
import pandas as pd
from sklearn import svm
from os import walk
import matplotlib.pyplot as plt
from transformers.TimeTransformer import TimeTransformer
from sklearn.preprocessing import StandardScaler

WINDOW = '15min'
#WINDOW = '30min'
#WINDOW = '1H'

prefixes = ['lu_', 'ma_', 'me_', 'je_', 've_', 'sa_', 'di_']
anomalies = ['lu_14', 'lu_15', 'lu_16', 'me_21', 'je_21', 've_7', 've_8']
data_acc_columns = [
        "bytesIn",
        "bytesOut",
        "eventDuration",
        "status",
        "nRequest",
        "cpuUsage",
        "memUsage",
        "swapUsage"
    ]

OUTPUT_FILE = "./outputs/result_svm_agg_window_%s.csv"%WINDOW
# NON-NOMINAL WEEK
VAL_FILE = "./outputs/val_%s/data_from_2022-06-20_to_2022-06-25_window_%s.pkl" % (WINDOW, WINDOW)

# NOMINAL WEEK
#VAL_FILE = "./outputs/val_%s/data_from_2022-06-27_to_2022-06-29_window_%s.pkl" % (WINDOW, WINDOW)

# TEST ON SAME DATASET
#VAL_FILE = "./outputs/concat_%s/data_from_2022-06-04_to_2022-06-14.pkl" % WINDOW

def time_decompose(df, columns, data_acc):
    for index, row in df.iterrows():
        if not "%d"%row['times'] in data_acc:
            data_acc["%d"%row['times']] = {}
            for column in columns:
                data_acc["%d"%row['times']][column] = []
        for column in columns:
            data_acc["%d"%row['times']][column].append(row[column])
            
def getMetricsForWindow(window, pickle_file_path):
    timeEncoded = "%d" % TimeTransformer.encode(window)
    df = pd.read_pickle(pickle_file_path)
    data = {}
    time_decompose(df, data_acc_columns, data)
    return data[timeEncoded]
            
# ============= MAIN ============= 
            
if __name__ == '__main__':
    files = []
    for (dirpath, dirnames, filenames) in walk("./outputs/concat_%s"%WINDOW):
        files.extend(filenames)
        break
    
    df_list = []
    
    for file in files:
        if file.endswith(".pkl"):
            print("loading %s"%file)
            df_list.append(pd.read_pickle("./outputs/concat_%s/%s"%(WINDOW, file)))
     
        
    data_acc = {}
    
    for df in df_list:
        time_decompose(df, data_acc_columns, data_acc)
    
    dataFrame_val = pd.read_pickle(VAL_FILE)
    
    data_acc_val = {}
    time_decompose(dataFrame_val, data_acc_columns, data_acc_val)
    
    output_content_file = []
    
    counters = {
        "true_positive": 0,
        "false_positive": 0,
        "true_negative": 0,
        "false_negative": 0,
        "total": 0
    }
    # Iterate over time windows (lu_8, lu_9, etc.)
    for a, b in zip([prefix for prefix in prefixes for i2 in range(0, 24)], [i for i in range(0, 24)]*7):    
        WATCH_HOUR = "%d" % TimeTransformer.encode("%s%s"%(a, b))
        HUMAN_RD_HOUR = "%s%s"%(a, b)
        
        if not WATCH_HOUR in data_acc or not WATCH_HOUR in data_acc_val:
            print("====== %s ======="%HUMAN_RD_HOUR)
            print("NO RECORD")
            print("")
            output_content_file.append([HUMAN_RD_HOUR, "NO RECORD"])
            continue
            
        dataFrame = pd.DataFrame(data_acc[WATCH_HOUR])
        dataFrameTestPCA = pd.concat([dataFrame, pd.DataFrame(data_acc_val[WATCH_HOUR])], ignore_index=True)
        dataFrameVal = pd.DataFrame(data_acc_val[WATCH_HOUR])
        
        pca = PCA(n_components=2)
        pca_data = pca.fit_transform(dataFrame)
        pca_dataTest = pca.transform(dataFrameTestPCA)
        
        pca_dataFrame = pd.DataFrame(data = pca_data
                     , columns = ['principal component 1', 'principal component 2'])
        pca_dataFrame_test = pd.DataFrame(data = pca_dataTest
                     , columns = ['principal component 1', 'principal component 2'])
        
        #pca_dataFrame.plot.scatter(x='principal component 1', y='principal component 2')
        #pca_dataFrame_test.plot.scatter(x='principal component 1', y='principal component 2')
        
        scaler = StandardScaler()
        oneClassSVM = svm.OneClassSVM(kernel='rbf', gamma=0.1, nu=0.1)
        oneClassSVM.fit(scaler.fit_transform(dataFrame.to_numpy()))
        scores = oneClassSVM.score_samples(scaler.fit_transform(dataFrame.to_numpy()))
        svmPredict = oneClassSVM.predict(scaler.fit_transform(dataFrameVal.to_numpy()))
        explained_variance_ratios = pca.explained_variance_ratio_
        
        print("====== %s ======="%HUMAN_RD_HOUR)
        print(dataFrame)
        print(dataFrameVal)
        print("CLASS PREDICTED => %d  |  Explained variance ratios => %s" % (svmPredict[0], explained_variance_ratios))
        print(scores)
        is_fault = True if HUMAN_RD_HOUR in anomalies else False
        result = ''
        counters['total'] += 1
        if svmPredict[0] == 1:
            if is_fault == True:
                result = 'FALSE NEGATIVE'
                counters['false_negative'] += 1
            else:
                result = 'TRUE NEGATIVE'
                counters['true_negative'] += 1
        else:
            if is_fault == True:
                result = 'TRUE POSITIVE'
                counters['true_positive'] += 1
            else:
                result = 'FALSE POSITIVE'
                counters['false_positive'] += 1
        output_content_file.append([HUMAN_RD_HOUR, "  OK  " if svmPredict[0] == 1 else "  ANOMALY  ", "  (%s)"%result])
        print("")

    # append scores
    output_content_file[0].append("  |  TRUE POSITIVES = %d  |  FALSE POSITIVES = %d  |  TRUE NEGATIVES = %d  |  FALSE NEGATIVES = %d"%(counters['true_positive'], counters['false_positive'], counters['true_negative'], counters['false_negative']))
    output_content_file[1].append("  |  Accuracy = %5.2f percent  |  Recall = %5.2f percent  |  Precision = %5.2f percent | total = %d"%((counters['true_positive']+counters['true_negative'])*100/counters['total'], counters['true_positive']*100/(counters['true_positive']+counters['false_negative']), counters['true_positive']*100/(counters['true_positive']+counters['false_positive']), counters['total']))
    
    with open(OUTPUT_FILE, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(output_content_file)
