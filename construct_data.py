# -*- coding: utf-8 -*-

import lib.tasELK as tasELK
import discoveryELK
import os
from pandas import DataFrame
from pandas.plotting import scatter_matrix
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline

from transformers.BytesOutTransformer import BytesOutTransformer
from transformers.BytesInTransformer import BytesInTransformer
from transformers.EventDurationTransformer import EventDurationTransformer
from transformers.NRequestTransformer import NRequestTransformer
from transformers.StatusTransformer import StatusTransformer
from transformers.TimeTransformer import TimeTransformer
from transformers.CpuUsageTransformer import CpuUsageTransformer
from transformers.ServiceTransformer import ServiceTransformer

USER = os.getenv('ELK_USER')
PWD = os.getenv('ELK_PWD')

LABELS_ENCODER_FILEPATH = './outputs/labels_encoder.npy'

WINDOW = '15min'
RESUSE_LABELS=True

fromDateStr = '2022-05-24T00:00:00'
toDateStr = '2022-06-03T23:59:59'

#fromDateStr = '2022-06-04T00:00:00'
#toDateStr = '2022-06-14T23:59:59'

#fromDateStr = '2022-06-15T00:00:00'
#toDateStr = '2022-06-19T23:59:59'

# === VAL ===
#fromDateStr = '2022-06-20T00:00:00'
#toDateStr = '2022-06-25T23:59:59'

#fromDateStr = '2022-06-27T00:00:00'
#toDateStr = '2022-06-29T23:59:59'

index_packetbeat = 'els_r_is_monitoring_packetbeat_eagle*'
index_metricbeat = 'els_r_is_monitoring_metricbeat_eagle*'

def scatterAndPlotDataFrame(dataFrameToPlot):
    # copy dataFrame
    dataFrameToPlot = dataFrameToPlot.copy()
    
    print (dataFrameToPlot["times"])
    
    dataFrameToPlot.hist(bins=50)
    scatter_matrix(dataFrameToPlot, figsize=(20, 10))
    plt.grid(True)
    fig, ax = plt.subplots(3, 2)
    ax[0, 0].scatter(dataFrameToPlot.index, dataFrameToPlot['bytesIn'], marker='.')
    ax[0, 1].scatter(dataFrameToPlot.index, dataFrameToPlot['bytesOut'], marker='.')
    ax[1, 0].scatter(dataFrameToPlot.index, dataFrameToPlot['eventDuration'], marker='.')
    ax[1, 1].scatter(dataFrameToPlot.index, dataFrameToPlot['status'], marker='.')
    ax[2, 0].scatter(dataFrameToPlot.index, dataFrameToPlot['cpuUsage'], marker='.')
    ax[2, 1].scatter(dataFrameToPlot.index, dataFrameToPlot['nRequest'], marker='.')
    fig.tight_layout()
    
    dataFrameToPlot = TimeTransformer(humanReadable=True).fit_transform(dataFrameToPlot)
    
    dataFrameToPlot.plot.scatter(x='times', y='bytesIn', rot=90)
    dataFrameToPlot.plot.scatter(x='times', y='bytesOut', rot=90)
    dataFrameToPlot.plot.scatter(x='times', y='eventDuration', rot=90)
    dataFrameToPlot.plot.scatter(x='times', y='status', rot=90)
    dataFrameToPlot.plot.scatter(x='times', y='cpuUsage', rot=90)
    dataFrameToPlot.plot.scatter(x='times', y='nRequest', rot=90)

def wrapToPandaDataframe(dataArray, columns = None, missingValue = 0):
    dataDict = {}
    if not columns == None and not '@timestamp' in columns:
        columns.append('@timestamp')
        
    for dataInstance in dataArray:
        for key in columns if not columns == None else dataInstance.keys():
            if not key in dataDict:
                dataDict[key] = []
            # Fill with dataInstance value if it exists, toherwise use the missingValue
            dataDict[key].append(dataInstance[key] if key in dataInstance.keys() else missingValue)
    keysToRemove = []
    for key in dataDict.keys():
        if len(dataDict[key]) < len(dataArray) or (columns != None and not key in columns):
            keysToRemove.append(key)
    print("Removing columns : ")
    print(keysToRemove)
    for key in keysToRemove:
        dataDict.pop(key)
    dataFrame = DataFrame(dataDict)
    dataFrame['@timestamp'] = pd.to_datetime(dataFrame['@timestamp']) # convert column to datetime object
    dataFrame.set_index('@timestamp', inplace=True) # set column 'date' to index
    return dataFrame.sort_index()

def extractDay(weekday):
    pass

# ======= MAIN ===========
if __name__ == "__main__":
    pipeline_packetbeat = Pipeline(
        [
             ("BytesOut", BytesOutTransformer()),
             ("BytesIn", BytesInTransformer()),
             ("EventDuration", EventDurationTransformer()),
             ("Status", StatusTransformer()),
             ("NRequest", NRequestTransformer()),
             ("Service", ServiceTransformer(reuse_labels=RESUSE_LABELS))
        ]
    )
    
    pipeline_metricbeat = Pipeline(
        [
             ("cpuUsage", CpuUsageTransformer()),
             ("memUsage", CpuUsageTransformer()),
             ("swapUsage", CpuUsageTransformer())
        ]
    )
    
    pipeline_post_retrievement = Pipeline(
        [
            ("times", TimeTransformer())
        ]
    )
    
    # =========================================================================
    
    esConnection = tasELK.ESConnect(
        hosts = ['https://elk.host:9200/'],
        cacertFilePath = 'E:\\Users\\milazzc\\AppData\\Roaming\\pip\\curl-ca-bundle-tas.crt',
        user = USER,
        password = PWD
    )
    docs = discoveryELK.getDataFromTo(
        esConnection,
        index_packetbeat,
        fromDateStr,
        toDateStr,
        matches={'destination.port': 8080, 'type': 'http'}
    )
    
    docs += discoveryELK.getDataFromTo(
        esConnection,
        index_packetbeat,
        fromDateStr,
        toDateStr,
        matches={'source.port': 8080, 'type': 'http'}
    )
    
    print("Got %d Hits for packetbeat data :" % len(docs))
    dataSet = wrapToPandaDataframe(pipeline_packetbeat.fit_transform(docs), columns = [ 'bytesIn', 'bytesOut', 'eventDuration', 'status', 'nRequest', 'service' ])
    
    # create service and duration correlation data set
    service_duration_dataFrame = dataSet[['service', 'eventDuration']].copy()
    
    dataFrame = dataSet.resample(WINDOW).agg({'nRequest': 'count', 'bytesIn': 'mean', 'bytesOut': 'mean', 'eventDuration': 'mean', 'status': 'mean'}).fillna(0)
    
    docs = discoveryELK.getDataFromTo(
        esConnection,
        index_metricbeat,
        fromDateStr,
        toDateStr,
        exists=["system.cpu.system.pct"]
    )
    
    print("Got %d Hits for metricbeat data :" % len(docs))
    dataSet = wrapToPandaDataframe(pipeline_metricbeat.fit_transform(docs), columns = [ 'cpuUsage', 'memUsage', 'swapUsage' ])
    # Merge packetebeat dataframe with metricbeat dataframe
    dataFrame['cpuUsage'] = dataSet.resample(WINDOW).mean().fillna(0)['cpuUsage']
    dataFrame['memUsage'] = dataSet.resample(WINDOW).mean().fillna(0)['memUsage']
    dataFrame['swapUsage'] = dataSet.resample(WINDOW).mean().fillna(0)['swapUsage']
    
    # Add the time aggregation to generalize daily behavior
    dataFrame = pipeline_post_retrievement.fit_transform(dataFrame)
    
    scatterAndPlotDataFrame(dataFrame)
    print(dataFrame)
    
    #Save dataFrame to file
    dataFrame.to_pickle("./outputs/data_from_%s_to_%s_window_%s.pkl"%(fromDateStr[0:10], toDateStr[0:10], WINDOW))
    service_duration_dataFrame.to_pickle("./outputs/service_data_from_%s_to_%s_window_%s.pkl"%(fromDateStr[0:10], toDateStr[0:10], WINDOW))
