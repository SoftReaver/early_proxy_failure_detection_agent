# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin


class EventDurationTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for row in X:
            if not 'duration' in row['event']:
                row['eventDuration'] = 1
            else:
                row['eventDuration'] = row['event']['duration']
        return X
