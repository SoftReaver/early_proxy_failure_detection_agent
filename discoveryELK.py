from datetime import datetime
from pandas import DataFrame

MAX_HITS = 10_000

def getDataFromTo(ELKConnection, index, fromDateStr, toDateStr, matches = {}, filters = {}, exists = []):
  format = "%Y-%m-%dT%H:%M:%S"
  fromDate = datetime.strptime(fromDateStr, format)
  toDate = datetime.strptime(toDateStr, format)
  lastDocumentsTimestamp = toDateStr # will be used at the very end to include the very last timestamp
  slideRange = toDate - fromDate
  windowedFromDate = 0
  
  documents = []
  
  # Loop until we scaned the whole range
  while True:
    # adjust the window width
    while True:
      windowedFromDate = toDate - slideRange
      if (windowedFromDate < fromDate):
        windowedFromDate = fromDate
      windowedFromDateStr = windowedFromDate.strftime(format)
      
      print ("Fetching hits count between %s and %s"%(windowedFromDateStr, toDateStr))
      resp = ELKConnection.getBetweenTimestamps(index, windowedFromDateStr, toDateStr, matches = matches, filters = filters, exists = exists, maxHits=0)
      print ("%d hits found"%resp['hits']['total']['value'])
      
      if resp['hits']['total']['value'] < MAX_HITS:
        print(" => window width is set to %s"%__datatimeDeltaToString__(slideRange))
        break
      else:
        print ("Reducing the window by two")
        # Reduce the window by 2
        slideRange /= 2
  
    print ("Getting data between >> %s and %s <<" % (windowedFromDateStr, toDateStr))
    resp = ELKConnection.getBetweenTimestamps(index, windowedFromDateStr, toDateStr, matches = matches, filters = filters, exists = exists)
    print ("Appending %d documents"%len(resp['hits']['hits']))
    print ('--------------------------------------')
    for doc in resp['hits']['hits']:
      documents.append(doc['_source'])
    
    if windowedFromDate == fromDate:
      break
    
    # slide dates
    toDate -= slideRange
    toDateStr = toDate.strftime(format)
    
    # Raising the window
    slideRange *= 2
  
  print (" => getting last documents on timestamp %s" % lastDocumentsTimestamp)
  # include the "toDate" timestamp
  resp = ELKConnection.getByTimestamp(index, lastDocumentsTimestamp, matches = matches, filters = filters, exists = exists)
  
  if len(resp['hits']['hits']) > 0:
    print ("adding last %d document(s)" % len(resp['hits']['hits']))
    for doc in resp['hits']['hits']:
      documents.append(doc['_source'])
  else:
    print ("No additional document to append")
  
  return documents

def __datatimeDeltaToString__(datatimeDelta):
  suffixes = ['millisecond', 'second', 'minute', 'hour', 'day', 'week']
  suffixesLimits = [1000, 60, 60, 24, 7]
  suffixIndice = 0
  total = round(datatimeDelta.total_seconds() * 1000)
  
  while True:
    if suffixIndice == len(suffixesLimits) or total <= suffixesLimits[suffixIndice]:
      break
    else:
      total /= suffixesLimits[suffixIndice]
      suffixIndice += 1

  return "%d %s"%(total, suffixes[suffixIndice]) if total < 2 else "%s %ss"%(total, suffixes[suffixIndice])


def wrapToPandaDataframe(dataArray):
    dataDict = {}
    for dataInstance in dataArray:
        for key in dataInstance.keys():
            if not key in dataDict:
                dataDict[key] = []
            dataDict[key].append(dataInstance[key])
    keysToRemove = []
    for key in dataDict.keys():
        if len(dataDict[key]) < len(dataArray):
            keysToRemove.append(key)
    for key in keysToRemove:
        dataDict.pop(key)
    return DataFrame(dataDict)


