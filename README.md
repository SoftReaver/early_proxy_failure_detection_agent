# Description
Ce dépot sert de "proof of concept" dans l'élaboration d'un agent détecteur d'anomalie précoce pour la maintenance préventive.
Un rapport écrit récapitulant les expérimentation et les résultats se trouve à la racine du projet sous le nom de `Rapport_expérimentation_agent_IA_détecteur_anomalies.pdf`.

# prérequis

Il est nécessaire d'avoir python et au moins un gestionnaire de dépendance (pip par exemple) d'installés.
Les bibliothèques suivante sont à installer :

```
pip install --upgrade scikit-learn==1.1.1
pip install --upgrade pandas==1.4.3
pip install --upgrade matplotlib==3.5.0
pip install seaborn
pip install numpy
pip install datetime
```

# L'arborescence
    \_lib/	                        <== Contient les librairies propres aux projet.
    \_outputs/		        <== Contient les jeux de donneés et les résultat (fichiers CSV) de prédiction des modèles.
    |   \_concat_<WINDOW>/             <== Les dossiers concat contiennent les jeux de données d'apprentissage extraits de la BDD ElasticSearch. Ces derniers sont strucurés pour travailler avec un nuage de points et par profil d'heure et jour de semaine.
    |   \_val_<WINDOW>/         <== Jeu de données de test.
    |   \_eventDurationData/    <== Contient également des dossiers de jeux de données d'apprentissage et de tests structurés pour la 3eme appoche abordé dans le chapitre 5 du rapport.
    \_transformers/		        <== Contient les transformers (sémantique sklearn) pour les pipeline de transformation des données.
    \_construct_data.py             <== Permet de générer les fichier de donées .pkl depuis les données brutes de ELK.
    \_discoveryELK.py               <== Utilisé par construct_data.py, sont rôle est de récupérer les données sur ELK entre deux date données, il assure le fenêtrage nécessaire pour pouvoir récupérer des données sur de large période.
    \_test_algo_aggregation.py      <== Résumé des données par heure et jour de semaine.
    \_test_algo_duration.py         <== Test avec la corrélation entre le service (url) et le temps de réponse.
    \_test_algo_features_dbscan.py  <== Test avec le nuage de point (toute les dimmenssions) et l'algo DBSCAN (meilleur résultat).
    \_test_algo_features_svm.py     <== Test avec le nuage de point aussi mais avec l'algo OneclassSVM.


# Lancer les scripts
Les scripts disponibles au lancement sont ceux qui commencent par `test_`.

:warning: NOTE : Il est inutile de tester le `construct_data.py`, en l'état il ne fonctionnera pas. Il faut au préalable configurer une stack ELK et avoir des données remontée par un metricbeat (module system) et un packetbeat (protocole HTTP). Il peut être également nécessaire de modifier les filtres utilisés, les paramères (ligne 130) et le nom des indexes (lignes 45 et 46).
