# -*- coding: utf-8 -*-
from numpy.random import randint
from sklearn.base import BaseEstimator, TransformerMixin

class StatusTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for row in X:
            row['status'] = 1 if row['status'] == 'OK' else 0
        return X
