# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin

class SwapUsageTransformer(BaseEstimator, TransformerMixin):
    
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for row in X:
            row['swapUsage'] = row['system']['memory']['swap']['used']['pct']
        return X
