from datetime import datetime
from elasticsearch import Elasticsearch
from dateutil import tz
import pytz

MAX_HITS_QTY=10000

class ESConnect:
      
  def __init__(self, hosts, user, password, cacertFilePath = ''):
    self.es = Elasticsearch(
      hosts=hosts,
      # make sure we verify SSL certificates
      verify_certs=True,
      # provide a path to CA certs on disk
      ca_certs=cacertFilePath,
      http_auth=(user, password)
    )

  def getByTimestamp(self, index, date, filters = {}, matches = {}, exists = [], maxHits = MAX_HITS_QTY):
    """Get documents for the given date.
 
     	Parameters:
        	index (string): The index pattern you want to fetch from
        	date (string): The date for which you want to get documents. Date format is &lt;year&gt;-&lt;month&gt;-&lt;day&gt;T&lt;hour&gt;:&lt;minute&gt;:&lt;second&gt; (e.g. '2022-06-06T9:17:0').
            (optional) exists (list): a list os string that defines fields you want to exists. Ex. ["foo", "foo.bar"]
            (optional) filters (hash): an dictionnary that contains key-value pairs where the key is the field and the value defines what you want to exclude from de result. Ex. { "foo": "bar", "multiple_value": ["could be this", "or that"] }
            (optional) matches (hash): an dictionnary that contains key-value pairs where the key is the field and the value defines what result(s) the filed should contain. Ex. { "width": 500, "height": [300, 400] }
            (optional) maxHits (integer): the maximum documents to be returned (maximum is 10_000 according to elasticsearch lib documentation). Default is 10_000

	Returns:
		A response object comming from the ElasticSearch python API. Structure is as :
	{
	   "took":3, <= Time in ms took by Elasticsearch to handle your query
	   "timed_out":false,
	   "_shards":{
	      "total":1, <= Total of shards used for the query
	      "successful":1,
	      "skipped":0,
	      "failed":0
	   },
	   "hits":{
	      "total":{
	         "value":1670,  <= The total of document found
	         "relation":"eq"
	      },
	      "max_score":0.0,
	      "hits":[
	            ... <= List of documents + metadata you are looking for (your data is stored under the _source field of each documents)
	        ]
	    }
	}

    """
    statement = {
      "bool": {
        "must": [
          {
            "term": {
              "@timestamp": {
                "value": ESConnect.__LocalDateIsoToUTCDateIso__(date)
              }
            }
          }
        ]
      }
    }
    self.__addFiltersStatement__(statement, filters)
    self.__addMatchesStatement__(statement, matches)
    self.__addExistsStatement__(statement, exists)
    return self.makeQuery(index, statement, maxHits)

  def getBetweenTimestamps(self, index, fromDate, toDate, filters = {}, matches = {}, exists = [], maxHits = MAX_HITS_QTY):
    """Get documents between given dates. NOTE : The fromDate is included but the toDate is <u>excluded</u>, this make easier to work with sliding time window.
 
     	Parameters:
        	index (string): The index pattern you want to fetch from
        	fromDate (string): The date from which you want to start searching for documents. Date format is &lt;year&gt;-&lt;month&gt;-&lt;day&gt;T&lt;hour&gt;:&lt;minute&gt;:&lt;second&gt; (e.g. '2022-06-06T9:17:0').
            fromDate (string): The date limit to which you want to retrieve documents. Date format is &lt;year&gt;-&lt;month&gt;-&lt;day&gt;T&lt;hour&gt;:&lt;minute&gt;:&lt;second&gt; (e.g. '2022-06-06T9:17:0').
            (optional) exists (list): a list os string that defines fields you want to exists. Ex. ["foo", "foo.bar"]
            (optional) filters (hash): an dictionnary that contains key-value pairs where the key is the field and the value defines what you want to exclude from de result. Ex. { "foo": "bar", "multiple_value": ["could be this", "or that"] }
            (optional) matches (hash): an dictionnary that contains key-value pairs where the key is the field and the value defines what result(s) the filed should contain. Ex. { "width": 500, "height": [300, 400] }
            (optional) maxHits (integer): the maximum documents to be returned (maximum is 10_000 according to elasticsearch lib documentation). Default is 10_000

	Returns:
		A response object comming from the ElasticSearch python API. Structure is as :
	{
	   "took":3, <= Time in ms took by Elasticsearch to handle your query
	   "timed_out":false,
	   "_shards":{
	      "total":1, <= Total of shards used for the query
	      "successful":1,
	      "skipped":0,
	      "failed":0
	   },
	   "hits":{
	      "total":{
	         "value":1670,  <= The total of document found
	         "relation":"eq"
	      },
	      "max_score":0.0,
	      "hits":[
	            ... <= List of documents + metadata you are looking for (your data is stored under the _source field of each documents)
	        ]
	    }
	}

    """
    statement = {
      "bool": {
        "filter": [
          {
            "range": {
              "@timestamp": {
                "gte": ESConnect.__LocalDateIsoToUTCDateIso__(fromDate),
                "lt": ESConnect.__LocalDateIsoToUTCDateIso__(toDate)
              }
            }
          }
        ]
      }
    }
    self.__addFiltersStatement__(statement, filters)
    self.__addMatchesStatement__(statement, matches)
    self.__addExistsStatement__(statement, exists)
    return self.makeQuery(index, statement, maxHits)
 
  def getLastDuration(self, index, duration, filters = {}, matches = {}, exists = [], maxHits = MAX_HITS_QTY):
    """Get documents for the given index for the las duration.
 
     	Parameters:
        	index (string): The index pattern you want to fetch from
        	duration (string): the duration is a string that follows the pattern &lt;number&gt;&lt;unit&gt; where unit may be one of [s, m, h, d, M, y]. Ex. 10m = 10 minutes, 3d = 3 days
            (optional) exists (list): a list os string that defines fields you want to exists. Ex. ["foo", "foo.bar"]
            (optional) filters (hash): an dictionnary that contains key-value pairs where the key is the field and the value defines what you want to exclude from de result. Ex. { "foo": "bar", "multiple_value": ["could be this", "or that"] }
            (optional) matches (hash): an dictionnary that contains key-value pairs where the key is the field and the value defines what result(s) the filed should contain. Ex. { "width": 500, "height": [300, 400] }
            (optional) maxHits (integer): the maximum documents to be returned (maximum is 10_000 according to elasticsearch lib documentation). Default is 10_000

	Returns:
		A response object comming from the ElasticSearch python API. Structure is as :
	{
	   "took":3, <= Time in ms took by Elasticsearch to handle your query
	   "timed_out":false,
	   "_shards":{
	      "total":1, <= Total of shards used for the query
	      "successful":1,
	      "skipped":0,
	      "failed":0
	   },
	   "hits":{
	      "total":{
	         "value":1670,  <= The total of document found
	         "relation":"eq"
	      },
	      "max_score":0.0,
	      "hits":[
	            ... <= List of documents + metadata you are looking for (your data is stored under the _source field of each documents)
	        ]
	    }
	}

    """
    statement = {
      "bool": {
        "filter": [
          {"range": {"@timestamp": {"gte": "now-%s"%duration, "lte": "now"}}}
        ]
      }
    }
    self.__addFiltersStatement__(statement, filters)
    self.__addMatchesStatement__(statement, matches)
    self.__addExistsStatement__(statement, exists)
    return self.makeQuery(index, statement, maxHits)

  def makeQuery(self, index, query, maxHits = MAX_HITS_QTY):
      return self.es.search(index=index, size=maxHits, query=query)

  def __LocalDateIsoToUTCDateIso__(dateString):
    format = "%Y-%m-%dT%H:%M:%S"
    localDate = datetime.strptime(dateString, format)
    localDate = localDate.replace(tzinfo=tz.tzlocal())
    zuluDate = localDate.astimezone(pytz.UTC)
    return zuluDate.strftime("%sZ"%format)

  def __UTCDateIsoToLocalDateIso__(dateString):
    format = "%Y-%m-%dT%H:%M:%S"
    zuluDate = datetime.strptime(dateString, format)
    zuluDate = zuluDate.replace(tzinfo=pytz.UTC)
    localDate = zuluDate.astimezone(tz.tzlocal())
    return localDate.strftime(format)

  def __addFiltersStatement__(self, statement, filters):
    if len(filters) > 0:
      if not 'bool' in statement:
        statement['bool'] = {}
      if not 'must_not' in statement['bool']:
        statement['bool']['must_not'] = []
      for key in filters:
        newTerm = {}
        newTerm[key] = filters[key]
        statement['bool']['must_not'].append({ "terms": newTerm } if isinstance(filters[key], list) else { "term": newTerm })
    return
      
  def __addMatchesStatement__(self, statement, matches):
    if len(matches) > 0:
      if not 'bool' in statement:
        statement['bool'] = {}
      if not 'must' in statement['bool']:
        statement['bool']['must'] = []
      for key in matches:
        newTerm = {}
        newTerm[key] = matches[key]
        statement['bool']['must'].append({ "terms": newTerm } if isinstance(matches[key], list) else { "term": newTerm })
    return

  def __addExistsStatement__(self, statement, exists):
    if len(exists) > 0:
      if not 'bool' in statement:
        statement['bool'] = {}
      if not 'must' in statement['bool']:
        statement['bool']['must'] = []
      for field in exists:
        if isinstance(field, str):
          statement['bool']['must'].append({ "exists": { "field": field }})
    return

  if __name__ == "__main__":
    print (__UTCDateIsoToLocalDateIso__(None, "2022-06-20T11:30:02"))
    pass