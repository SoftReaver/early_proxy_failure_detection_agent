# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin

class MemUsageTransformer(BaseEstimator, TransformerMixin):
    
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for row in X:
            row['memUsage'] = row['system']['memory']['actual']['used']['pct']
        return X
