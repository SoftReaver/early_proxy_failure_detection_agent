
def anomaly_detector_scorer(y_true, y_predicted):
    score = 0
    for index, pred in enumerate(y_predicted):
        score += scorePrediction(pred, y_true, index, y_predicted)
    return score    

def isGoodPredict(pred, y_val, index, y_pred):
    return True if scorePrediction(pred, y_val, index, y_pred) >= 0 else False
    
def scorePrediction(pred, y_val, index, y_pred):
    if pred == -1:
        if pred == y_val[index]:
            return 1
        elif (index < len(y_val)-1 and pred == y_val[index+1]):
            return 1.5
        elif (index < len(y_val)-2 and pred == y_val[index+2]):
            return 2
        elif (index < len(y_val)-3 and pred == y_val[index+3]):
            return 3
        elif (index < len(y_val)-4 and pred == y_val[index+4]):
            return 4
        elif (index < len(y_val)-5 and pred == y_val[index+5]):
            return 5
        elif (index < len(y_val)-6 and pred == y_val[index+6]):
            return 6
        elif (index < len(y_val)-7 and pred == y_val[index+7]):
            return 7
        elif (index < len(y_val)-8 and pred == y_val[index+8]):
            return 8
        elif (index > 0 and pred == y_val[index-1]):
            return 0.5
        else:
            return -2 # False alert
    else:
        if pred == y_val[index]:
            return 1
        else:
            if (index > 0 and y_pred[index-1] == -1):
                return 1
            elif (index > 1 and y_pred[index-2] == -1):
                return 1
            elif (index > 2 and y_pred[index-3] == -1):
                return 1
            elif (index > 3 and y_pred[index-4] == -1):
                return 1
            elif (index > 4 and y_pred[index-5] == -1):
                return 1
            elif (index > 5 and y_pred[index-6] == -1):
                return 1
            elif (index > 6 and y_pred[index-7] == -1):
                return 1
            elif (index > 7 and y_pred[index-8] == -1):
                return 1
            return -1