# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
from sklearn import preprocessing
    
LABELS_ENCODER_FILEPATH = './outputs/labels_encoder.npy'

class ServiceTransformer(BaseEstimator, TransformerMixin):
    
    def __init__(self, reuse_labels=False):
        self.reuse_labels = reuse_labels
    
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        le = preprocessing.LabelEncoder()
        labels = []
        for row in X:
            if 'url' in row and 'path' in row['url']:
                row['service'] = row['url']['path']
            else:
                row['service'] = ''
                
            if not row['service'] in labels:
                labels.append(row['service'])
                
        if self.reuse_labels == False:
            le.fit(labels)
        else: # Reuse an existing dict
            le.classes_ = np.load(LABELS_ENCODER_FILEPATH)
        
        for row in X:
            # if no correspondance, update the encoder
            if not row['service'] in le.classes_:
                # add the correspondance in the classes_ array
                le.classes_ = np.append(le.classes_, row['service'])
                
            row['service'] = le.transform([row['service']])[0]
            
        # Save the dict for reuse
        np.save(LABELS_ENCODER_FILEPATH, le.classes_)

        return X
    