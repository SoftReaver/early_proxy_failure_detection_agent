# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin


class BytesOutTransformer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        for row in X:
            if row['destination']['port'] == 8080:
                row['bytesOut'] = row['source']['bytes'] if 'bytes' in row['source'] else 0
            else:
                row['bytesOut'] = row['destination']['bytes'] if 'bytes' in row['destination'] else 0
        return X
