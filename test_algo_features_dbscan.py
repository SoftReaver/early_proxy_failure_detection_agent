# -*- coding: utf-8 -*-
from cProfile import label
import numpy as np
import csv
from sklearn.decomposition import PCA
import pandas as pd
from sklearn.cluster import DBSCAN
from os import walk
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from pandas.plotting import scatter_matrix
from test_algo_aggregation import getMetricsForWindow
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer
import seaborn as sn
from lib.AnomalyDetectorScorer import anomaly_detector_scorer, isGoodPredict

WINDOW = '15min'
#WINDOW = '30min'
#WINDOW = '1H'

N_COMPONENTS = 4

OUTPUT_FILE = "./outputs/result_DBSCAN_window_%s_ncomp_%d.csv"%(WINDOW, N_COMPONENTS)
# NON-NOMINAL WEEK
VAL_FILE = "./outputs/val_%s/data_from_2022-06-20_to_2022-06-25_window_%s.pkl" % (WINDOW, WINDOW)

# NOMINAL WEEK
#VAL_FILE = "./outputs/val_%s/data_from_2022-06-27_to_2022-06-29_window_%s.pkl" % (WINDOW, WINDOW)

# TEST ON SAME DATASET
#VAL_FILE = "./outputs/concat_%s/data_from_2022-06-04_to_2022-06-14.pkl" % WINDOW

files = []
for (dirpath, dirnames, filenames) in walk("./outputs/concat_%s"%WINDOW):
    files.extend(filenames)
    break

df_list = []

for file in files:
    if file.endswith(".pkl"):
        print("loading %s"%file)
        df_list.append(pd.read_pickle("./outputs/concat_%s/%s"%(WINDOW, file)))
            
def column_decompose(df, columns, data_acc):
    for index, row in df.iterrows():
        for col in columns:
            if not col in data_acc:
                data_acc[col] = []
            data_acc[col].append(row[col])
    
def returnMergedAnomalies(a1, a2):
    ret = {}
    for col in data_acc_columns:
        ret[col] = a1[col]
        ret[col].extend(a2[col])
    return ret

def annotate(ax, dataFrame, comp1, comp2, annotations, isUnderneeth = False):
    inv_pos = -1 if isUnderneeth == True else 1
    duplicate = round(len(dataFrame) / len(annotations))
    new_annotations = []
    for annot in annotations:
        for i in range(0, duplicate):
            new_annotations.append(annot)
    for (index, annotation) in enumerate(new_annotations):
        ax.annotate(annotation, xy=(dataFrame[comp1][index], dataFrame[comp2][index]), xytext=(dataFrame[comp1][index]+0.5*inv_pos, dataFrame[comp2][index]+0.5*inv_pos))
 
data_acc = {}
data_acc_columns = [
    "bytesIn",
    "bytesOut",
    "eventDuration",
    "status",
    "nRequest",
    "cpuUsage",
    "memUsage",
    "swapUsage"
]

timestamps = []

for df in df_list:
    timestamps.extend(df.index.to_list())
    column_decompose(df, data_acc_columns, data_acc)
    
df = pd.DataFrame(data_acc)
scatter_matrix(df, figsize=(20, 10))

pca = PCA(n_components=len(data_acc_columns))
scale_pca_pipeline = Pipeline([
    ('scaler', StandardScaler()),
    ('pca', pca)
])
pca_data = scale_pca_pipeline.fit_transform(df)

elbow_values = [
    [i for i in range(0, len(pca.explained_variance_ratio_))],
    list(pca.explained_variance_ratio_)
]
pca_ratios = pca.explained_variance_ratio_
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(elbow_values[0][:], elbow_values[1][:])
ax.plot([i for i in range(0, len(pca.explained_variance_ratio_))], np.zeros(shape=(len(pca.explained_variance_ratio_), 1)), 'r-')
plt.ylabel('explained ratio')
plt.xlabel('principal components')
plt.show()
pca_dataFrame = pd.DataFrame(data = pca_data
                 , columns = ["principal component %d"%i for i in range(1, len(data_acc_columns)+1)])
    
dataFrame_val = pd.read_pickle(VAL_FILE)
timestamps.extend(dataFrame_val.index.to_list())

data_acc_val = {}
column_decompose(dataFrame_val, data_acc_columns, data_acc_val)

pca_data_val = scale_pca_pipeline.transform(pd.DataFrame(data_acc_val))
pca_dataframe_val = pd.DataFrame(data = pca_data_val,
                                 columns = ["principal component %d"%i for i in range(1, len(data_acc_columns)+1)])

anomalies = returnMergedAnomalies(getMetricsForWindow('lu_14', VAL_FILE),
              returnMergedAnomalies(getMetricsForWindow('lu_15', VAL_FILE), 
                returnMergedAnomalies(getMetricsForWindow('lu_16', VAL_FILE),
                  returnMergedAnomalies(getMetricsForWindow('me_21', VAL_FILE),
                    returnMergedAnomalies(getMetricsForWindow('ve_0', VAL_FILE),
                      returnMergedAnomalies(getMetricsForWindow('ve_9', VAL_FILE),
                        getMetricsForWindow('ve_10', VAL_FILE)
                      )
                    )
                  )
                )
              )
            )

# get points around above anomalies
near_anomalies = returnMergedAnomalies(getMetricsForWindow('lu_12', VAL_FILE),
              returnMergedAnomalies(getMetricsForWindow('lu_13', VAL_FILE), 
                returnMergedAnomalies(getMetricsForWindow('lu_17', VAL_FILE),
                  returnMergedAnomalies(getMetricsForWindow('lu_18', VAL_FILE),
                    returnMergedAnomalies(getMetricsForWindow('me_19', VAL_FILE),
                      returnMergedAnomalies(getMetricsForWindow('me_20', VAL_FILE),
                        returnMergedAnomalies(getMetricsForWindow('me_22', VAL_FILE),
                          returnMergedAnomalies(getMetricsForWindow('me_23', VAL_FILE),
                            returnMergedAnomalies(getMetricsForWindow('ve_1', VAL_FILE),
                              returnMergedAnomalies(getMetricsForWindow('ve_2', VAL_FILE),
                                returnMergedAnomalies(getMetricsForWindow('ve_7', VAL_FILE),
                                  returnMergedAnomalies(getMetricsForWindow('ve_8', VAL_FILE),
                                    returnMergedAnomalies(getMetricsForWindow('ve_11', VAL_FILE),
                                      getMetricsForWindow('ve_12', VAL_FILE)
                                    )
                                  )
                                )
                              )
                            )
                          )
                        )
                      )
                    )
                  )
                )
              )
            )

annotations = ['lu_14', 'lu_15', 'lu_16', 'me_21', 've_0', 've_9', 've_10']
near_anomalies_annotations = ['lu_12', 'lu_13', 'lu_17', 'lu_18', 'me_19', 'me_20', 'me_22', 'me_23', 've_1', 've_2', 've_7', 've_8', 've_11', 've_12']

df_anomalies = pd.DataFrame(data=anomalies)
df_near_anomalies = pd.DataFrame(data=near_anomalies)
pca_data_anomalies = scale_pca_pipeline.transform(pd.DataFrame(df_anomalies))
pca_data_near_anomalies = scale_pca_pipeline.transform(pd.DataFrame(df_near_anomalies))
pca_dataframe_anomalies = pd.DataFrame(data = pca_data_anomalies,
                                 columns = ["principal component %d"%i for i in range(1, len(data_acc_columns)+1)])
pca_dataframe_near_anomalies = pd.DataFrame(data = pca_data_near_anomalies,
                                 columns = ["principal component %d"%i for i in range(1, len(data_acc_columns)+1)])

data_ax1 = pca_dataFrame.plot.scatter(x='principal component 1', y='principal component 2', c = 'green', label='nominal')
pca_dataframe_val.plot.scatter(x='principal component 1', y='principal component 2', ax = data_ax1, c = 'blue', label='validation')
pca_dataframe_near_anomalies.plot.scatter(x='principal component 1', y='principal component 2', ax = data_ax1, c = 'purple', label='near anomalies')
#annotate(data_ax1, pca_dataframe_near_anomalies, 'principal component 1', 'principal component 2', near_anomalies_annotations)
pca_dataframe_anomalies.plot.scatter(x='principal component 1', y='principal component 2', ax = data_ax1, c = 'red', label='anomalies')
#annotate(data_ax1, pca_dataframe_anomalies, 'principal component 1', 'principal component 2', annotations)
data_ax1.legend(loc ="lower right"); 

data_ax2 = pca_dataFrame.plot.scatter(x='principal component 2', y='principal component 3', c = 'green', label='nominal')
pca_dataframe_val.plot.scatter(x='principal component 2', y='principal component 3', ax = data_ax2, c = 'blue', label='validation')
pca_dataframe_near_anomalies.plot.scatter(x='principal component 2', y='principal component 3', ax = data_ax2, c = 'purple', label='near anomalies')
#annotate(data_ax2, pca_dataframe_near_anomalies, 'principal component 2', 'principal component 3', near_anomalies_annotations)
pca_dataframe_anomalies.plot.scatter(x='principal component 2', y='principal component 3', ax = data_ax2, c = 'red', label='anomalies')
#annotate(data_ax2, pca_dataframe_anomalies, 'principal component 2', 'principal component 3', annotations)
data_ax2.legend(loc ="lower right"); 

data_ax3 = pca_dataFrame.plot.scatter(x='principal component 1', y='principal component 3', c = 'green', label='nominal')
pca_dataframe_val.plot.scatter(x='principal component 1', y='principal component 3', ax = data_ax3, c = 'blue', label='validation')
pca_dataframe_near_anomalies.plot.scatter(x='principal component 1', y='principal component 3', ax = data_ax3, c = 'purple', label='near anomalies')
#annotate(data_ax3, pca_dataframe_near_anomalies, 'principal component 1', 'principal component 3', near_anomalies_annotations)
pca_dataframe_anomalies.plot.scatter(x='principal component 1', y='principal component 3', ax = data_ax3, c = 'red', label='anomalies')
#annotate(data_ax3, pca_dataframe_anomalies, 'principal component 1', 'principal component 3', annotations)
data_ax3.legend(loc ="lower right"); 

plt.show()
# =========================== MODEL ===========================
eps = []
eps.extend((np.asarray(range(1, 9))).tolist())

min_samples = []
min_samples.extend((np.asarray(range(5, 20))).tolist())

param_grid= [
    {
      'eps': eps,
      'min_samples': min_samples,
      'metric': [ 'euclidean' ]
    }
]
dbscan = DBSCAN()
pca = PCA(n_components=N_COMPONENTS)
scale_pca_pipeline = Pipeline([
    ('scaler', StandardScaler()),
    ('pca', pca)
])
pca_data_set = scale_pca_pipeline.fit_transform(df)
df_data_set = pd.DataFrame(pca_data_set)

pca_data_val = scale_pca_pipeline.transform(pd.DataFrame(data_acc_val))
df_data_val = pd.DataFrame(pca_data_val)

index_offset = len(df_data_set)

# merge data set and validation set
df_data_set = pd.concat([df_data_set, df_data_val], ignore_index=True)

# create class set
anomalies_times = [
        '2022-06-20 14:00:00',
        '2022-06-20 15:15:00',
        '2022-06-22 18:15:00',
        '2022-06-23 21:15:00',
        '2022-06-24 7:00:00',
        '2022-06-24 7:15:00'
    ]
anomalies_index = []
for anomaly_time in anomalies_times:
    try:
        anomalies_index.append(dataFrame_val.index.get_loc(anomaly_time) + index_offset)
    except:
        pass # Do nothing if no index found for the given time
        
y_val = np.ones(shape=len(df_data_set))
for index in anomalies_index:
    y_val[index] *= -1

#print(sklearn.metrics.get_scorer_names())
#print(sorted(sklearn.neighbors.VALID_METRICS['brute']))
grid_search = GridSearchCV(dbscan, param_grid, cv=5, scoring=make_scorer(anomaly_detector_scorer, greater_is_better=True))
grid_search.fit(df_data_set, y_val)
print('_____________')
print(grid_search.best_estimator_)
print(grid_search.best_score_)
print(grid_search.best_params_)
print("explained ratios :")
print(pca_ratios)

# CONFUSION MATRIX
eps, min_samples, metric = (grid_search.best_params_['eps'], grid_search.best_params_['min_samples'], grid_search.best_params_['metric'])
classifier = DBSCAN(eps=eps, min_samples=min_samples,metric=metric)
y_pred = classifier.fit_predict(df_data_set)

confusion_matrix = pd.crosstab(y_val, y_pred, rownames=['Actual'], colnames=['Predicted'], margins = True)
sn.heatmap(confusion_matrix, annot=True)

df_data_set.plot.scatter(x=0, y=1, c = y_pred, cmap='inferno')

plt.show()

output_content_file = []
counters = {
    "true_positive": 0,
    "false_positive": 0,
    "true_negative": 0,
    "false_negative": 0,
    "total": 0
}
for val, pred, index in zip(y_val, y_pred, range(0, len(y_pred))):
    pred = 1 if pred >= 0 else -1
    counters['total'] += 1
    result = ''
    if pred == -1:
        if isGoodPredict(pred, y_val, index, y_pred):
            result = 'TRUE POSITIVE'
            counters['true_positive'] += 1
        else:
            result = 'FALSE POSITIVE'
            counters['false_positive'] += 1
    else:
        if isGoodPredict(pred, y_val, index, y_pred):
            result = 'TRUE NEGATIVE'
            counters['true_negative'] += 1
        else:
            result = 'FALSE NEGATIVE'
            counters['false_negative'] += 1
    output_content_file.append([timestamps[index], '  ANOMALY  ' if pred == -1 else '  OK  ', "  (%s)"%result])

# append scores
output_content_file[0].append("  |  TRUE POSITIVES = %d  |  FALSE POSITIVES = %d  |  TRUE NEGATIVES = %d  |  FALSE NEGATIVES = %d"%(counters['true_positive'], counters['false_positive'], counters['true_negative'], counters['false_negative']))
output_content_file[1].append("  |  Accuracy = %5.2f percent  |  Recall = %5.2f percent  |  Precision = %5.2f percent | total = %d"%((counters['true_positive']+counters['true_negative'])*100/counters['total'], counters['true_positive']*100/(counters['true_positive']+counters['false_negative']), counters['true_positive']*100/(counters['true_positive']+counters['false_positive']), counters['total']))
output_content_file[2].append(str(grid_search.best_params_))

with open(OUTPUT_FILE, 'w') as f:
    writer = csv.writer(f)
    writer.writerows(output_content_file)
