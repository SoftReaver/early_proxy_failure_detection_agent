# -*- coding: utf-8 -*-
from sklearn.base import BaseEstimator, TransformerMixin
from datetime import date, datetime
from dateutil import tz
import pytz
import re

class TimeTransformer(BaseEstimator, TransformerMixin):
    
    weekDayPrefixes = ['lu_', 'ma_', 'me_', 'je_', 've_', 'sa_', 'di_']
    
    def __init__(self, humanReadable = False):
        self.humanReadable = humanReadable
    
    def fit(self, X, y=None):
        
        return self

    def transform(self, X, y=None):
        def mapFoncteur(row):
            m = re.match(r"(\d{2,4})-(\d{1,2})-(\d{1,2})\s+(\d+?):", str(row))
            (yearStr, monthStr, dayStr, hourStr) = (m.group(1), m.group(2), m.group(3), m.group(4))
            dt = datetime(int(yearStr), int(monthStr), int(dayStr), int(hourStr), 0, 0).replace(tzinfo=pytz.UTC).astimezone(tz.tzlocal())
            weekDay = date(dt.year, dt.month, dt.day).weekday()
            hour = dt.hour
            return "%s%s"%(self.weekDayPrefixes[weekDay], hour) if self.humanReadable == True else (weekDay * 24 + hour)
        
        times = list(map(mapFoncteur, X.index))
        X['times'] = times
        return X

    def encode(value):
        m = re.match(r"(\w{2}_)(\d{1,2})", str(value))
        (prefix, hourStr) = (m.group(1), m.group(2))
        hour = int(hourStr)
        weekDay = TimeTransformer.weekDayPrefixes.index(prefix)
        return (weekDay * 24 + hour)
